using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public Vector2 inputVec;                    // 입력된 이동 벡터
    public float speed;                         // 이동 속도
    public Scanner scanner;                     // 스캐너 컴포넌트
    public Hand[] hands;                        // 손 컴포넌트 배열
    public RuntimeAnimatorController[] animCon; // 런타임 애니메이터 컨트롤러 배열

    Rigidbody2D rigid;                          // Rigidbody2D 컴포넌트
    SpriteRenderer spriter;                     // SpriteRenderer 컴포넌트
    Animator anim;                              // Animator 컴포넌트

    void Awake()
    {
        // 컴포넌트 초기화
        rigid = GetComponent<Rigidbody2D>();            // Rigidbody2D 컴포넌트 초기화
        spriter = GetComponent<SpriteRenderer>();       // SpriteRenderer 컴포넌트 초기화
        anim = GetComponent<Animator>();                // Animator 컴포넌트 초기화
        scanner = GetComponent<Scanner>();              // 스캐너 컴포넌트 초기화
        hands = GetComponentsInChildren<Hand>(true);    // 자식 오브젝트에서 Hand 컴포넌트들을 가져옴
    }

    void OnEnable()
    {
        // 속도를 캐릭터의 속도에 맞게 조정
        speed *= Character.Speed;
        // 애니메이터의 컨트롤러를 플레이어 ID에 맞게 설정
        anim.runtimeAnimatorController = animCon[GameManager.instance.playerId];
    }

    void Update()
    {
        // 게임이 실행 중이 아니면 처리하지 않음
        if (!GameManager.instance.isLive)
            return;
    }

    private void FixedUpdate()
    {
        // 게임이 실행 중이 아니면 처리하지 않음
        if (!GameManager.instance.isLive)
            return;

        // 다음 위치 벡터를 계산하여 이동
        Vector2 nextVec = inputVec * speed * Time.fixedDeltaTime;

        // 위치 이동
        rigid.MovePosition(rigid.position + nextVec);
    }

    void OnMove(InputValue value)
    {
        // 입력된 이동값을 가져옴
        inputVec = value.Get<Vector2>();
    }

    void LateUpdate()
    {
        // 게임이 실행 중이 아니면 처리하지 않음
        if (!GameManager.instance.isLive)
            return;

        // 애니메이터의 "Speed" 파라미터 설정
        anim.SetFloat("Speed", inputVec.magnitude);

        // 입력된 이동값에 따라 스프라이트를 좌우로 뒤집음
        if (inputVec.x != 0)
        {
            spriter.flipX = inputVec.x < 0;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        // 게임이 실행 중이 아니면 처리하지 않음
        if (!GameManager.instance.isLive)
            return;

        // 체력 감소 처리
        GameManager.instance.health -= Time.deltaTime * 10;

        // 체력이 0 이하면 게임 오버 처리
        if (GameManager.instance.health < 0)
        {
            // 자식 오브젝트 중 2번째부터 비활성화 처리
            for (int index = 2; index < transform.childCount; index++)
            {
                transform.GetChild(index).gameObject.SetActive(false);
            }
            // 애니메이터의 "Dead" 트리거 설정 및 게임 오버 호출
            anim.SetTrigger("Dead");
            GameManager.instance.GameOver();
        }
    }
}
