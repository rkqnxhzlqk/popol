using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;                             // 이동속도
    public float health;                            // 현재 체력
    public float maxHealth;                         // 최대 체력
    public RuntimeAnimatorController[] animCon;     // 애니메이터 컨트롤러 배열
    public Rigidbody2D target;                      // 목표 타깃

    bool isLive;    // 생존 여부

    Rigidbody2D rigid;          // Rigidbody2D 컴포넌트
    Collider2D coll;            // Collider2D 컴포넌트
    Animator anim;              // Animator 컴포넌트
    SpriteRenderer spriter;     // SpriteRenderer 컴포넌트
    WaitForFixedUpdate wait;    // WaitForFixedUpdate 객체

    private void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();        // 자신의 Rigidbody2D 컴포넌트 가져오기
        coll = GetComponent<Collider2D>();          // 자신의 Collider2D 컴포넌트 가져오기
        anim = GetComponent<Animator>();            // 자신의 Animator 컴포넌트 가져오기
        spriter = GetComponent<SpriteRenderer>();   // 자신의 SpriteRenderer 컴포넌트 가져오기
        wait = new WaitForFixedUpdate();            // WaitForFixedUpdate 객체 생성
    }

    private void FixedUpdate()
    {
        // 게임이 실행 중이지 않거나 적이 살아있지 않거나 "Hit" 애니메이션 상태에 있는 경우 반환
        if (!GameManager.instance.isLive)
            return;

        if (!isLive || anim.GetCurrentAnimatorStateInfo(0).IsName("Hit") )
            return;

        // 대상으로 향하는 방향 벡터를 계산하고 해당 방향으로 이동
        Vector2 dirVec = target.position - rigid.position;
        Vector2 nextVec = dirVec.normalized * speed * Time.fixedDeltaTime;
        rigid.MovePosition(rigid.position + nextVec);
        rigid.velocity = Vector2.zero;
    }

    private void LateUpdate()
    {
        // 게임이 실행 중이지 않거나 적이 살아있지 않은 경우 반환
        if (!GameManager.instance.isLive)
            return;

        if (!isLive)
            return;

        // 대상의 위치에 따라 스프라이트를 수평으로 뒤집기
        spriter.flipX = target.position.x < rigid.position.x;
    }

    void OnEnable()
    {
        // 대상을 플레이어의 Rigidbody2D로 설정
        target = GameManager.instance.player.GetComponent<Rigidbody2D>();
        isLive = true;
        coll.enabled = true;
        rigid.simulated = true;
        spriter.sortingOrder = 2;
        anim.SetBool("Dead", false); ;
        health = maxHealth;
    }

    public void Init(SpawnData data)
    {
        // 런타임 애니메이터 컨트롤러, 속도, 최대 체력을 스폰 데이터에 기반하여 설정
        anim.runtimeAnimatorController = animCon[data.spriteType];
        speed = data.speed;
        maxHealth = data.health;
        health = data.health;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // 충돌이 총알이 아니거나 적이 살아있지 않은 경우 반환
        if (!collision.CompareTag("Bullet") || !isLive)
            return;

        // 충돌한 객체의 "Bullet" 컴포넌트에서 가져온 데미지 값을 현재 체력에서 빼는 연산을 수행
        health -= collision.GetComponent<Bullet>().damage;
        StartCoroutine(KnockBack());

        if (health > 0)
        {
            // "Hit" 애니메이션을 재생하고 해당되는 효과음 재생
            anim.SetTrigger("Hit");
            AudioManager.instance.PlaySfx(AudioManager.Sfx.Hit);
        }
        else
        {
            // 적이 죽음
            isLive = false;
            coll.enabled = false;
            rigid.simulated = false;
            spriter.sortingOrder = 1;
            anim.SetBool("Dead", true);
            GameManager.instance.kill++;
            GameManager.instance.GetExp();

            if(GameManager.instance.isLive)
                AudioManager.instance.PlaySfx(AudioManager.Sfx.Dead);
        }
    }

    IEnumerator KnockBack()
    {
        yield return wait; // 다음 하나의 물리 프레임을 딜레이
        Vector3 playerPos = GameManager.instance.player.transform.position;
        Vector3 dirVec = transform.position - playerPos;
        rigid.AddForce(dirVec.normalized * 3, ForceMode2D.Impulse);
    }

    void Dead()
    {
        gameObject.SetActive(false);
    }
}
