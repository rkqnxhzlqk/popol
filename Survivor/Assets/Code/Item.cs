using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public ItemData data;   // 아이템 데이터
    public int level;       // 아이템 레벨
    public Weapon weapon;   // 무기 컴포넌트
    public Gear gear;       // 장비 컴포넌트

    Image icon;             // 아이템 아이콘 이미지
    Text textLevel;         // 아이템 레벨 텍스트
    Text textName;          // 아이템 이름 텍스트
    Text textDesc;          // 아이템 서명 텍스튼

    void Awake()
    {
        // 아이콘 이미지와 텍스트 컴포넌트 가져오기
        icon = GetComponentsInChildren<Image>()[1];
        icon.sprite = data.itemIcon;

        Text[] texts = GetComponentsInChildren<Text>();
        textLevel = texts[0];
        textName = texts[1];
        textDesc = texts[2];
        textName.text = data.itemName;
    }

    void OnEnable()
    {
        // 아이템 레벨 텍스트 설정
        textLevel.text = "LV" + (level + 1);

        // 아이템 종류에 따라 설명 텍스트 설정
        switch (data.itemType)
        {
            case ItemData.ItemType.Melee:
            case ItemData.ItemType.Range:
                textDesc.text = string.Format(data.itemDesc, data.damages[level] * 100, data.counts[level]);
                break;
            case ItemData.ItemType.Glove:
            case ItemData.ItemType.Shoe:
                textDesc.text = string.Format(data.itemDesc, data.damages[level] * 100);
                break;
            default:
                textDesc.text = string.Format(data.itemDesc);
                break;
        }

       
    }

    public void OnClick()
    {
        // 아이템 종류에 따라 동작 설정
        switch (data.itemType)
        {
            case ItemData.ItemType.Melee:
            case ItemData.ItemType.Range:
                if(level == 0)
                {
                    // 무기 생성 및 초기화
                    GameObject newWeapon = new GameObject();
                    weapon = newWeapon.AddComponent<Weapon>();
                    weapon.Init(data);
                }
                else
                {
                    // 다음 레벨에 따른 데미지와 개수 설정
                    float nextDamage = data.baseDamage;
                    int nextCount = 0;

                    nextDamage += data.baseDamage * data.damages[level];
                    nextCount += data.counts[level];

                    // 무기 레벨 업
                    weapon.LevelUp(nextDamage, nextCount);
                }
                level++;
                break;
            case ItemData.ItemType.Glove:
            case ItemData.ItemType.Shoe:
                if (level == 0)
                {
                    // 장비 생성 및 초기화
                    GameObject newGear = new GameObject();
                    gear = newGear.AddComponent<Gear>();
                    gear.Init(data);
                }
                else
                {
                    // 다음 레벨에 따른 속도 증가율 설정
                    float nextRate = data.damages[level];
                    gear.LevelUp(nextRate);
                }
                level++;
                break;
            case ItemData.ItemType.Heal:
                // 체력 회복
                GameManager.instance.health = GameManager.instance.maxhealth;
                break;
        }

        // 아이템의 레벨이 최대 레벨에 도달한 경우 버튼 비활성화
        if (level == data.damages.Length)
        {
            GetComponent<Button>().interactable = false;
        }
    }
}
