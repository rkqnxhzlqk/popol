using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance; // 싱글톤 인스턴스
    [Header("# Game Control")]
    public bool isLive;                 // 게임 진행 여부
    public float gameTime;              // 게임 경과 시간
    public float maxGameTime = 2 * 10f; // 최대 게임 시간
    [Header("# Player Info")]
    public int playerId;                // 플레이어 ID
    public float health;                // 플레이어 체력
    public float maxhealth = 100;       // 최대 체력
    public int level;                   // 플레이어 레벨
    public int kill;                    // 킬 수
    public int exp;                     // 경험치
    public int[] nextExp = { 3, 5, 10, 100, 150, 210, 280, 360, 450, 600 }; // 다음 레벨에 필요한 경험치 배열
    [Header("# Game Object")]
    public PoolManager pool;            // 오브젝트 풀 매니저
    public Player player;               // 플레이어 객체
    public LevelUp uiLevelUp;           // 레벨 업 UI
    public Result uiResult;             // 결과 UI
    public Transform uiJoy;             // 조이스틱 UI
    public GameObject enemyCleaner;     // 적 클리너 오브젝트

    void Awake()
    {
        instance = this;                    // 인스턴스 설정
        Application.targetFrameRate = 60;   // 프레임 속도 설정
    }

    public void GameStart(int id)
    {
        playerId = id;      // 플레이어 ID 설정
        health = maxhealth; // 체력 초기화

        player.gameObject.SetActive(true);  // 플레이어 활성화
        uiLevelUp.Select(playerId % 2);     // 레벨 업 UI 선택
        Resume();                           // 일시정지 해제

        AudioManager.instance.PlayBgm(true);                    // 배경음 재생
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Select); // 선택 사운드 재생
    }

    public void GameOver()
    {
        StartCoroutine(GameOverRoutine());
    }

    IEnumerator GameOverRoutine()
    {
        isLive = false; // 생존 상태를 false로 설정

        yield return new WaitForSeconds(0.5f);

        uiResult.gameObject.SetActive(true);    // 결과 UI 활성화
        uiResult.Lose();                        // 패배 결과 표시
        Stop();                                 // 일시정지

        AudioManager.instance.PlayBgm(false);                   // 배경음 정지
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Lose);   // 패배 사운드 재생
    }

    public void GameVictroy()
    {
        StartCoroutine(GameVictroyRoutine());
    }

    IEnumerator GameVictroyRoutine()
    {
        isLive = false;                 // 생존 상태를 false로 설정
        enemyCleaner.SetActive(true);   // 적 클리너 활성화

        yield return new WaitForSeconds(0.5f);

        uiResult.gameObject.SetActive(true);    // 결과 UI 활성화
        uiResult.Win();                         // 승리 결과 표시
        Stop();                                 // 일시정지

        AudioManager.instance.PlayBgm(false);                   // 배경음 정지
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Win);    // 승리 사운드 재생
    }

    public void GameRetry()
    {
        SceneManager.LoadScene(0);  // 게임 재시작을 위해 Scene을 다시 불러옴
    }

    public void GameQuit()
    {
        Application.Quit(); // 어플리케이션 종료
    }

    void Update()
    {
        if (!isLive)
            return; // 게임이 종료되었으면 업데이트 중단

        gameTime += Time.deltaTime; // 경과 시간 증가

        if (gameTime > maxGameTime)
        {
            gameTime = maxGameTime;
            GameVictroy();  // 게임 시간이 최대 게임 시간을 초과하면 승리 처리
        }
    }

    public void GetExp()
    {
        if (!isLive)
            return; // 게임이 종료되었으면 경험치 획득 중단

        exp++;      // 경험치 증가

        if (exp == nextExp[Mathf.Min(level, nextExp.Length-1)])
        {
            level++;            // 레벨 증가
            exp = 0;            // 경험치 초기화
            uiLevelUp.Show();   // 레벨 업 UI 표시
        }
    }

    public void Stop()
    {
        isLive = false;                     // 생존 상태를 false로 설정
        Time.timeScale = 0;                 // 게임 시간 흐름을 정지
        uiJoy.localScale = Vector3.zero;    // 조이스틱 UI 비활성화
    }

    public void Resume()
    {
        isLive = true;                  // 생존 상태를 true로 설정
        Time.timeScale = 1;             // 게임 시간 흐름을 정상으로 설정
        uiJoy.localScale = Vector3.one; // 조이스틱 UI 활성화
    }
}
