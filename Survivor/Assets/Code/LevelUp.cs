using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp : MonoBehaviour
{
    RectTransform rect; // UI의 RectTransform 컴포넌트
    Item[] items;       // 하위에 있는 모든 아이템 배열

    void Awake()
    {
        rect = GetComponent<RectTransform>();           // RectTransform 컴포넌트 가져오기
        items = GetComponentsInChildren<Item>(true);    // 하위에 있는 모든 Item 컴포넌트 가져오기
    }

    public void Show()
    {
        Next();                                                     // 다음 아이템 표시
        rect.localScale = Vector3.one;                              // UI 활성화
        GameManager.instance.Stop();                                // 게임 일시정지
        AudioManager.instance.PlaySfx(AudioManager.Sfx.LevelUp);    // 레벨 업 효과음 재생
        AudioManager.instance.EffextBgm(true);                      // 배경음 효과 적용
    }

    public void Hide()
    {
        rect.localScale = Vector3.zero;                             // UI 비활성화
        GameManager.instance.Resume();                              // 게임 재개
        AudioManager.instance.PlaySfx(AudioManager.Sfx.Select);     // 선택 효과음 재생
        AudioManager.instance.EffextBgm(false);                     // 배경음 효과 해제
    }

    public void Select(int index)
    {
        items[index].OnClick(); // 선택한 아이템을 클릭 처리
    }

    void Next()
    {
        // 1. 모든 아이템 비활성화
        foreach (Item item in items)
        {
            item.gameObject.SetActive(false);
        }
        // 2. 그 중에서 랜덤 3개 아이템 활성화
        int[] ran = new int[3];
        while (true)
        {
            ran[0] = Random.Range(0, items.Length);
            ran[1] = Random.Range(0, items.Length);
            ran[2] = Random.Range(0, items.Length);

            if (ran[0] != ran[1] && ran[1] != ran[2] && ran[0] != ran[2])
                break;
        }

        for(int index=0; index < ran.Length; index++)
        {
            Item ranItem = items[ran[index]];

            // 3. 만렙 아이템의 경우는 소비아이템으로 대체
            if (ranItem.level == ranItem.data.damages.Length)
            {
                items[4].gameObject.SetActive(true);
            }
            else
            {
                ranItem.gameObject.SetActive(true);
            }
        }        
    }
}
