using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Scriptble Object/ItemData")]
public class ItemData : ScriptableObject
{
    public enum ItemType { Melee, Range, Glove, Shoe, Heal }

    [Header("# Main Info")]
    public ItemType itemType;   // 아이템의 종류
    public int itemId;          // 아이템 식별자
    public string itemName;     // 아이템 이름
    [TextArea]
    public string itemDesc;     // 아이템 설명
    public Sprite itemIcon;     // 아이템 아이콘 이미지

    [Header("# Level Data")]
    public float baseDamage;    // 기본 공격력
    public int baseCount;       // 기본 개수
    public float[] damages;     // 레벨별 공격력
    public int[] counts;        // 레벨별 개수

    [Header("# Weapon")]
    public GameObject projectile;   // 발사체 프리팹
    public Sprite hand;             // 손 이미지
}
