using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [Header("#BGM")]
    public AudioClip bgmClip;      // 배경 음악 클립
    public float bgmVolume;        // 배경 음악 볼륨
    AudioSource bgmPlayer;         // 배경 음악 플레이어
    AudioHighPassFilter bgmEffect; // 배경 음악 이펙트

    [Header("#SFX")]
    public AudioClip[] sfxClips;    // 효과음 클립 배열
    public float sfxVolume;         // 효과음 볼륨
    public int channels;            // 효과음 플레이어 채널 수
    AudioSource[] sfxPlayers;       // 효과음 플레이어 배열
    int channelIndex;               // 현재 플레이 중인 채널 인덱스

    public enum Sfx { Dead, Hit, LevelUp=3, Lose, Melee, Range=7, Select, Win }
    // 효과음 종류를 정의한 열거형

    void Awake()
    {
        instance = this;    // AudioManager 인스턴스 설정
        Init();             // 초기화 함수 호출
    }

    void Init()
    {
        // 배경음 플레이어 초기화
        GameObject bgmObiect = new GameObject("BgmPlayer");
        bgmObiect.transform.parent = transform;
        bgmPlayer = bgmObiect.AddComponent<AudioSource>();
        bgmPlayer.playOnAwake = false;
        bgmPlayer.volume = bgmVolume;
        bgmPlayer.clip = bgmClip;
        bgmEffect = Camera.main.GetComponent<AudioHighPassFilter>();

        // 효과음 플레이어 초기화
        GameObject sfxObiect = new GameObject("SfxPlayer");
        sfxObiect.transform.parent = transform;
        sfxPlayers = new AudioSource[channels + 1];

        for (int index=0; index < sfxPlayers.Length; index++)
        {
            sfxPlayers[index] = sfxObiect.AddComponent<AudioSource>();
            sfxPlayers[index].playOnAwake = false;
            sfxPlayers[index].bypassListenerEffects = true;
            sfxPlayers[index].volume = sfxVolume;
        }

    }

    public void PlayBgm(bool isPlay)
    {
        if(isPlay)
        {
            bgmPlayer.Play();   // 배경 음악 재생
        }
        else
        {
            bgmPlayer.Stop();   // 배경 음악 정지
        }
    }

    public void EffextBgm(bool isPlay)
    {
        bgmEffect.enabled = isPlay; // 배경 음악 이펙트 활성화/비활성화
    }

    public void PlaySfx(Sfx sfx)
    {
        for(int index=0; index < sfxPlayers.Length; index++)
        {
            int loopIndex = (index + channelIndex) % sfxPlayers.Length;

            if (sfxPlayers[loopIndex].isPlaying)
                continue;

            int ranIndex = 0;
            if(sfx == Sfx.Hit || sfx == Sfx.Melee)
            {
                ranIndex = Random.Range(0, 2);
            }

            channelIndex = loopIndex;       // 현재 플레이 중인 채널 인덱스 업데이트
            sfxPlayers[loopIndex].clip = sfxClips[(int)sfx]; // 해당 채널에 효과음 클립 할당    
            sfxPlayers[loopIndex].Play();   // 효과음 재생
            break;  // 채널 선택 완료, 루프 종료
        }       
    }
}
