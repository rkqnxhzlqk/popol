using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reposition : MonoBehaviour
{
    Collider2D coll;

    private void Awake()
    {
        // 컴포넌트 초기화
        coll = GetComponent<Collider2D>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // 충돌한 오브젝트의 태그가 "Area"가 아니면 처리하지 않음
        if (!collision.CompareTag("Area"))
            return;

        // 플레이어와 현재 오브젝트의 위치를 가져옴
        Vector3 playerPos = GameManager.instance.player.transform.position;
        Vector3 myPos = transform.position;


        // 현재 오브젝트의 태그에 따라 처리 분기
        switch (transform.tag)
        {
            case "Ground":
                // 플레이어와 현재 오브젝트의 위치 차이를 계산
                float diffX = playerPos.x - myPos.x;
                float diffY = playerPos.y - myPos.y;
                float dirX = diffX < 0 ? -1 : 1;    // 플레이어와 오브젝트 사이의 방향을 결정하는 값
                float dirY = diffY < 0 ? -1 : 1;
                diffX = Mathf.Abs(diffX);
                diffY = Mathf.Abs(diffY);

                // X축과 Y축 위치 차이를 비교하여 이동 처리
                if (diffX > diffY)
                {
                    transform.Translate(Vector3.right * dirX * 40); // 오브젝트를 오른쪽 또는 왼쪽으로 이동
                }
                else if(diffX < diffY)
                {
                    transform.Translate(Vector3.up * dirY * 40);    // 오브젝트를 위쪽 또는 아래쪽으로 이동
                }
                break;

            case "Enemy":
                if(coll.enabled)
                {
                    // 플레이어와 현재 오브젝트의 위치 차이를 계산하여 이동 처리
                    Vector3 dist = playerPos - myPos;
                    Vector3 ran = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0);
                    transform.Translate(ran + dist * 2);    // 오브젝트를 일정 범위 내에서 플레이어 쪽으로 이동
                }
                break;
        }
    }

}
