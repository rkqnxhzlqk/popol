using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float damage;    // 총알의 데미지
    public int per;         // 충돌 횟수 제한

    Rigidbody2D rigid;      // Rigidbody2D 컴포넌트

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();    // Rigidbody2D 컴포넌트 가져오기
    }

    public void Init(float damage, int per, Vector3 dir)
    {
        this.damage = damage;   // 데미지 값 설정
        this.per = per;         // 충돌 횟수 제한 값 설정

        if (per >= 0)
        {
            rigid.velocity = dir * 15f; // 주어진 방향으로 초기 속도 설정
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Enemy") || per == -100)
            return;

        per--;  // 충돌 횟수 제한 감소

        if (per < 0)
        {
            rigid.velocity = Vector2.zero;  // 속도를 0으로 설정하여 정지
            gameObject.SetActive(false);    // 총알 비활성화
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Area") || per == -100)
            return;

        gameObject.SetActive(false);       // 총알이 영역을 벗어나면 비활성화
    }
}
