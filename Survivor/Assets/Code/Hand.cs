using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public bool isLeft;             // 무기가 왼쪽에 위치하는지 여부를 결정하는 변수
    public SpriteRenderer spriter;  // 스프라이트 렌더러 컴포넌트

    SpriteRenderer player;          // 플레이어 스프라이트 렌더러 컴포넌트

    Vector3 rightPos = new Vector3 (0.35f, -0.15f, 0);          // 오른쪽 위치
    Vector3 rightPosReverse = new Vector3(-0.15f, -0.15f, 0);   // 뒤집힌 오른쪽 위치
    Quaternion leftRot = Quaternion.Euler(0, 0, -35);           // 왼쪽 회전값
    Quaternion leftRotReverse = Quaternion.Euler(0, 0, -135);   // 뒤집힌 왼쪽 회전값

    void Awake()
    {
        // 부모 오브젝트에서 플레이어 스프라이트 렌더러 컴포넌트 가져오기
        player = GetComponentsInParent<SpriteRenderer>()[1];
    }

    void LateUpdate()
    {
        bool isReverse = player.flipX;  // 플레이어가 뒤집혀 있는지 여부 확인

        if (isLeft)
        {
            // 근거리 무기
            transform.localRotation = isReverse ? leftRotReverse : leftRot; // 무기 회전 설정
            spriter.flipY = isReverse;                                      // 스프라이트 Y 축 뒤집기 설정
            spriter.sortingOrder = isReverse ? 4 : 6;                       // 스프라이트 렌더링 순서 설정
        }
        else
        {
            // 원거리 무기
            transform.localPosition = isReverse ? rightPosReverse : rightPos;   // 무기 위치 설정
            spriter.flipX = isReverse;                                          // 스프라이트 X 축 뒤집기 설정
            spriter.sortingOrder = isReverse ? 6 : 4;                           // 스프라이트 렌더링 순서 설정
        }
    }
}
