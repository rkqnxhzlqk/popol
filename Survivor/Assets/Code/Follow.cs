using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    RectTransform rect; // RectTransform 컴포넌트

    void Awake()
    {
        rect = GetComponent<RectTransform>();   // RectTransform 컴포넌트 가져오기
    }

    void FixedUpdate()
    {
        // 플레이어의 위치를 화면 좌표로 변환하여 rect의 위치에 설정합니다.
        rect.position = Camera.main.WorldToScreenPoint(GameManager.instance.player.transform.position);  
    }
}
