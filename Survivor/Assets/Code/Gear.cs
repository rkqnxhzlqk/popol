using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear : MonoBehaviour
{
    public ItemData.ItemType type;  // 아이템 유형
    public float rate;              // 변화율

    public void Init(ItemData data)
    {
        // Basic Set
        name = "Gear" + data.itemId;                                // 아이템 이름 설정
        transform.parent = GameManager.instance.player.transform;   // 부모 설정
        transform.localPosition = Vector3.zero;                     // 로컬 위치 초기화

        // Property Set
        type = data.itemType;   // 아이템 유형 설정
        rate = data.damages[0]; // 변화율 설정
        ApplyGear();            // Gear 적용
    }

    public void LevelUp(float rate)
    {
        this.rate = rate;   // 변화율 설정
        ApplyGear();        // Gear 적용
    }

    void ApplyGear()
    {
        switch (type)
        {
            case ItemData.ItemType.Glove:
                RateUp();   // 변화율 증가
                break;
            case ItemData.ItemType.Shoe:
                SpeedUp();  // 속도 증가
                break;
        }
    }

    void RateUp()
    {
        // 부모 객체에서 Weapon 컴포넌트를 가진 자식 객체들을 가져옴
        Weapon[] weapons = transform.parent.GetComponentsInChildren<Weapon>();

        foreach(Weapon weapon in weapons)
        {
            switch(weapon.id)
            {
                case 0: // 아이템 ID가 0인 경우 (Rotate 타입)
                    float speed = 150 * Character.WeaponSpeed;  // 초기 속도 설정
                    weapon.speed = speed + (speed * rate);      // 속도 증가
                    break;
                default:    // 기타 아이템 ID (Fire 타입)
                    speed = 0.5f * Character.WeaponRate;  // 초기 속도 설정
                    weapon.speed = speed * (1f - rate);   // 속도 감소
                    break;
            }
        }
    }

    void SpeedUp()
    {
        float speed = 3 * Character.Speed;                          // 초기 속도 설정
        GameManager.instance.player.speed = speed + speed * rate;   // 속도 증가
    }
}
