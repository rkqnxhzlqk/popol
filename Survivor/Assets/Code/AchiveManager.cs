using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AchiveManager : MonoBehaviour
{
    public GameObject[] lockCharacter;
    public GameObject[] unlockCharacter;
    public GameObject uiNotice;

    // 열거형으로 업적(Achievement) 정의
    enum Achive { UnlockPotao, UnlockBean }
    Achive[] achives;
    WaitForSecondsRealtime wait;

    void Awake()
    {
        achives = (Achive[])Enum.GetValues(typeof(Achive));
        wait = new WaitForSecondsRealtime(5);

        if (!PlayerPrefs.HasKey("MyData"))
        {
            Init(); // 초기화 함수 호출
        }
    }

    void Init()
    {
        PlayerPrefs.SetInt("MyData", 1);

        // 모든 업적을 0으로 초기화
        foreach (Achive achive in achives)
        {
            PlayerPrefs.SetInt(achive.ToString(), 0);
        }
    }

    void Start()
    {
        UnlockCharacter(); // 캐릭터 잠금 해제 함수 호출
    }

    void UnlockCharacter()
    {
        // 모든 캐릭터에 대해 실행
        for (int index = 0; index < lockCharacter.Length; index++)
        {
            string achiveName = achives[index].ToString();
            bool isUnlock = PlayerPrefs.GetInt(achiveName) == 1;
            lockCharacter[index].SetActive(!isUnlock);  // 잠금 상태의 캐릭터를 비활성화
            unlockCharacter[index].SetActive(isUnlock); // 잠금 해제된 캐릭터를 활성화
        }
    }

    void LateUpdate()
    {
        // 업적을 확인하기 위해 늦은 업데이트에서 실행
        foreach (Achive achive in achives)
        {
            CheckAchive(achive); // 업적 확인 함수 호출
        }
    }

    void CheckAchive(Achive achive)
    {
        bool isAhive = false;

        switch (achive)
        {
            case Achive.UnlockPotao: 
                isAhive = GameManager.instance.kill >= 10; // 킬 카운트가 10 이상이면 업적 달성
                break;
            case Achive.UnlockBean:
                isAhive = GameManager.instance.gameTime == GameManager.instance.maxGameTime; // 게임 시간이 최대 게임 시간과 동일하면 업적 달성
                break;
        }

        // 업적을 달성하고 아직 해당 업적이 해금되지 않았을 경우
        if (isAhive && PlayerPrefs.GetInt(achive.ToString())==0)
        {
            PlayerPrefs.SetInt(achive.ToString(), 1); // 해당 업적을 해금 상태로 설정

            // UI 알림을 업적에 맞게 표시
            for (int index = 0; index < uiNotice.transform.childCount; index++)
            {
                bool isActive = index == (int)achive;
                uiNotice.transform.GetChild(index).gameObject.SetActive(isActive);
            }

            StartCoroutine(NoticeRoutine()); // 일정 시간 동안 UI 알림을 표시하는 코루틴 시작
        }
    }

    IEnumerator NoticeRoutine()
    {
        uiNotice.SetActive(true); // UI 알림 활성화
        AudioManager.instance.PlaySfx(AudioManager.Sfx.LevelUp); // 레벨 업 효과음을 재생합니다.

        yield return wait; // 일정 시간 동안 대기합니다.

        uiNotice.SetActive(false); // UI 알림을 비활성화합니다.
    }
}
