using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public enum InfoType { Exp, Level, Kill, Time, Health }
    public InfoType type;   // 정보 타입

    Text myText;        // 텍스트 컴포넌트
    Slider mySlider;    // 슬라이더 컴포넌트

    void Awake()
    {
        myText = GetComponent<Text>();      // 텍스트 컴포넌트 가져오기
        mySlider = GetComponent<Slider>();  // 슬라이더 컴포넌트 가져오기
    }

    void LateUpdate()
    {
        switch(type)
        {
            case InfoType.Exp:
                // 경험치 정보 업데이트
                float curExp = GameManager.instance.exp;
                float maxExp = GameManager.instance.nextExp[Mathf.Min(GameManager.instance.level, GameManager.instance.nextExp.Length -1)];
                mySlider.value = curExp / maxExp;
                break;

            case InfoType.Level:
                // 레벨 정보 업데이트
                myText.text = string.Format("Lv.{0:F0}", GameManager.instance.level);
                break;

            case InfoType.Kill:
                // 킬 수 정보 업데이트
                myText.text = string.Format("{0:F0}", GameManager.instance.kill);
                break;

            case InfoType.Time:
                // 남은 시간 정보 업데이트
                float remainTime = GameManager.instance.maxGameTime - GameManager.instance.gameTime;
                int min = Mathf.FloorToInt(remainTime / 60);
                int sec = Mathf.FloorToInt(remainTime % 60);
                myText.text = string.Format("{0:D2}:{1:D2}", min, sec);
                break;

            case InfoType.Health:
                // 체력 정보 업데이트
                float curhealth = GameManager.instance.health;
                float maxhealth = GameManager.instance.maxhealth;
                mySlider.value = curhealth / maxhealth;
                break;
        }
    }
}
