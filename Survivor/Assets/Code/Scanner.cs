using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scanner : MonoBehaviour
{
    public float scanRange;         // 탐지 범위    
    public LayerMask targetLayer;   // 타겟 레이어
    public RaycastHit2D[] targets;  // 탐지된 대상들
    public Transform nearestTarget; // 가장 가까운 대상

    void FixedUpdate()
    {
        // 현재 위치를 기준으로 주어진 범위 내에서 targetLayer에 속하는 모든 Collider를 탐지합니다.
        targets = Physics2D.CircleCastAll(transform.position, scanRange, Vector2.zero, 0, targetLayer);
        nearestTarget = GetNearest();   // 가장 가까운 대상을 가져옵니다.
    }

    Transform GetNearest()
    {
        Transform result = null;
        float diff = 100;   // 초기 차이값을 큰 값으로 설정합니다.

        foreach (RaycastHit2D target in targets)
        {
            Vector3 myPos = transform.position;
            Vector3 targetPos = target.transform.position;
            float curDiff = Vector3.Distance(myPos, targetPos); // 현재 대상과의 거리를 계산합니다.

            if (curDiff < diff)
            {
                diff = curDiff;             // 현재 대상과의 거리를 차이값으로 업데이트합니다.
                result = target.transform;  // 현재 대상을 가장 가까운 대상으로 설정합니다.
            }
        }

        return result;  // 가장 가까운 대상을 반환합니다.
    }
}
