using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    /// <summary>
    /// 게임 플레이어의 속도 속성을 나타냅니다.
    /// </summary>
    public static float Speed
    {
        get { return GameManager.instance.playerId == 0 ? 1.1f : 1f; }
    }

    /// <summary>
    /// 게임 플레이어의 무기 속도 속성을 나타냅니다.
    /// </summary>
    public static float WeaponSpeed
    {
        get { return GameManager.instance.playerId == 1 ? 1.1f : 1f; }
    }

    /// <summary>
    /// 게임 플레이어의 무기 발사 속도 속성을 나타냅니다.
    /// </summary>
    public static float WeaponRate
    {
        get { return GameManager.instance.playerId == 0 ? 0.9f : 1f; }
    }

    /// <summary>
    /// 게임 플레이어의 공격력 속성을 나타냅니다.
    /// </summary>
    public static float Damage
    {
        get { return GameManager.instance.playerId == 2 ? 1.2f : 1f; }
    }

    /// <summary>
    /// 게임 플레이어의 발사체 개수 속성을 나타냅니다.
    /// </summary>
    public static float Count
    {
        get { return GameManager.instance.playerId == 3 ? 1 : 0; }
    }
}
