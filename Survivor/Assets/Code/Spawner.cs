using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform[] spawnPoint;  // 적 캐릭터가 스폰될 위치
    public SpawnData[] spawnData;   // 스폰 데이터 배열
    public float levelTime;         // 레벨 시간

    int level;
    float timer;

    void Awake()
    {
        // 자식 객체의 Transform 컴포넌트들을 가져와 spawnPoint 배열에 할당
        spawnPoint = GetComponentsInChildren<Transform>();
        // 레벨 시간 계산
        levelTime = GameManager.instance.maxGameTime / spawnData.Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.isLive)
            return;

        timer += Time.deltaTime;
        level = Mathf.Min(Mathf.FloorToInt(GameManager.instance.gameTime / 10f), spawnData.Length -1);  // 현재 레벨 설정

        if (timer > spawnData[level].spawnTime)
        {            
            timer = 0f;
            Spawn();    // 스폰 함수 호출
        }
    }

    void Spawn()
    {
        // 적 캐릭터 오브젝트 풀에서 오브젝트 가져오기
        GameObject enemy = GameManager.instance.pool.Get(0);

        // 랜덤한 스폰 위치 설정
        enemy.transform.position = spawnPoint[Random.Range(1, spawnPoint.Length)].position;

        // 적 캐릭터 초기화
        enemy.GetComponent<Enemy>().Init(spawnData[level]);
    }
}

[System.Serializable]
public class SpawnData
{
    public float spawnTime; // 스폰 시간
    public int spriteType;  // 스프라이트 타입
    public int health;      // 체력
    public float speed;     // 속도
}