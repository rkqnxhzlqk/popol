using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public int id;          // 아이템 ID
    public int prefabId;    // 프리펩 ID
    public float damage;    // 데미지
    public int count;       // 발사 횟수
    public float speed;     // 발사 속도

    float timer;            // 발사 시간 간격을 체크하기 위한 타이머
    Player player;          // 플레이어 참조를 위한 변수

    void Awake()
    {
        player = GameManager.instance.player;   // GameManager에서 플레이어를 가져와 player 변수에 할당
    }

    void Update()
    {
        if (!GameManager.instance.isLive)
            return;

        switch (id)
        {
            case 0:
                transform.Rotate(Vector3.back * speed * Time.deltaTime);    // 아이템 ID가 0인 경우 회전하는 동작을 수행
                break;
            default:
                timer += Time.deltaTime;

                if(timer > speed)
                {
                    timer = 0;
                    Fire(); // 발사 함수 호출  
                }
                break;
        }

        // 테스트 코드
        if (Input.GetButtonDown("Jump"))
        {
            LevelUp(10, 1); // 테스트용 레벨업 함수 호출
        }
    }
    public void LevelUp(float damage, int count)
    {
        this.damage = damage * Character.Damage;
        this.count = count;

        if(id == 0)
            Batch();    // 아이템 ID가 0인 경우 Batch 함수 호출

        // ApplyGear 메시지 브로드캐스팅
        player.BroadcastMessage("ApplyGear", SendMessageOptions.DontRequireReceiver);
    }

    public void Init(ItemData data)
    {
        // 베이직 셋
        name = "Weapon" + data.itemId;
        transform.parent = player.transform;
        transform.localPosition = Vector3.zero;

        // 프로퍼티 셋
        id = data.itemId;                                   // 아이템 ID 설정
        damage = data.baseDamage * Character.Damage;        // 데미지 설정
        count = (int)(data.baseCount + Character.Count);    // 발사 횟수 설정

        for(int index = 0; index < GameManager.instance.pool.prefabs.Length; index++)
        {
            if(data.projectile == GameManager.instance.pool.prefabs[index])
            {
                prefabId = index;
                break;
            }
        }

        switch (id)
        {
            case 0:
                speed = 150 * Character.WeaponSpeed;    // 아이템 ID가 0인 경우, 속도 설정
                Batch();                                // 아이템 ID가 0인 경우 Batch 함수 호출
                break;
            default:
                speed = 0.5f * Character.WeaponRate;    // 속도 설정
                break;
        }

        // Hand Set
        Hand hand = player.hands[(int)data.itemType];   // 해당 아이템 유형에 따른 손 모델 설정
        hand.spriter.sprite = data.hand;                // 손 이미지 설정
        hand.gameObject.SetActive(true);                // 손 활성화

        player.BroadcastMessage("ApplyGear", SendMessageOptions.DontRequireReceiver);   // ApplyGear 메시지 브로드캐스팅
    }

    void Batch()
    {
        for(int index=0; index < count; index++)
        {
            Transform bullet; 
                
            if(index < transform.childCount)
            {
                bullet = transform.GetChild(index);
            }
            else
            {
                bullet = GameManager.instance.pool.Get(prefabId).transform; // 풀에서 총알 오브젝트 가져오기
                bullet.parent = transform;
            }

            

            bullet.localPosition = Vector3.zero;
            bullet.localRotation = Quaternion.identity;

            Vector3 rotVec = Vector3.forward * 360 * index / count;
            bullet.Rotate(rotVec);
            bullet.Translate(bullet.up * 1.5f, Space.World);
            bullet.GetComponent<Bullet>().Init(damage, -100, Vector3.zero); // 총알 초기화: 데미지, 관통 여부, (방향 -100은 무한 관통)
        }
    }
    
    void Fire()
    {
        if (!player.scanner.nearestTarget)
            return;

        Vector3 targetPos = player.scanner.nearestTarget.position;
        Vector3 dir = targetPos - transform.position;
        dir = dir.normalized;

        Transform bullet = GameManager.instance.pool.Get(prefabId).transform;   // 풀에서 총알 오브젝트 가져오기
        bullet.position = transform.position;
        bullet.rotation = Quaternion.FromToRotation(Vector3.up, dir);
        bullet.GetComponent<Bullet>().Init(damage, count, dir);                 // 총알 초기화: 데미지, 발사 횟수, 방향

        AudioManager.instance.PlaySfx(AudioManager.Sfx.Range);                  // 사운드 재생
    }
}
