using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Result : MonoBehaviour
{
    public GameObject[] titles;     // 패배 및 승리 텍스트 오브젝트 배열

    public void Lose()
    {
        titles[0].SetActive(true);  // 패배 텍스트를 활성화합니다.
    }

    public void Win()
    {
        titles[1].SetActive(true);  // 승리 텍스트를 활성화합니다.
    }
}
