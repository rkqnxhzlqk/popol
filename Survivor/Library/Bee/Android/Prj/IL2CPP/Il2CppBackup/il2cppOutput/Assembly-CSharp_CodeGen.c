﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AchiveManager::Awake()
extern void AchiveManager_Awake_m3B62144FD46525FA94B4EC9584FF5E77AF6F4F8D (void);
// 0x00000002 System.Void AchiveManager::Init()
extern void AchiveManager_Init_mDFD2CDB5E6E20EA55AEC14C6F14887F79C1C6C89 (void);
// 0x00000003 System.Void AchiveManager::Start()
extern void AchiveManager_Start_mF90E8828B0B06C403907F623928F8F712D437458 (void);
// 0x00000004 System.Void AchiveManager::UnlockCharacter()
extern void AchiveManager_UnlockCharacter_m1106130B02AD9E0E6C75602406502743E67F8E05 (void);
// 0x00000005 System.Void AchiveManager::LateUpdate()
extern void AchiveManager_LateUpdate_m6185BD47C1D9A1EABB210AF2C256C2B53E05BA3D (void);
// 0x00000006 System.Void AchiveManager::CheckAchive(AchiveManager/Achive)
extern void AchiveManager_CheckAchive_mA81C9732BE5A68EDC84ADF3CFB77A9C02F424353 (void);
// 0x00000007 System.Collections.IEnumerator AchiveManager::NoticeRoutine()
extern void AchiveManager_NoticeRoutine_m8C5A17AAF84709775BCDF66F6E5AF8C1A7268D0A (void);
// 0x00000008 System.Void AchiveManager::.ctor()
extern void AchiveManager__ctor_mD0BE170E9BF1B624D5DF0A31716E115C42FD1022 (void);
// 0x00000009 System.Void AchiveManager/<NoticeRoutine>d__12::.ctor(System.Int32)
extern void U3CNoticeRoutineU3Ed__12__ctor_m77585D04350EEC243A8D5E588307EBFD4A5A885A (void);
// 0x0000000A System.Void AchiveManager/<NoticeRoutine>d__12::System.IDisposable.Dispose()
extern void U3CNoticeRoutineU3Ed__12_System_IDisposable_Dispose_mAF2D87299483271E4BEF82A6776AEF2B1E07AC0B (void);
// 0x0000000B System.Boolean AchiveManager/<NoticeRoutine>d__12::MoveNext()
extern void U3CNoticeRoutineU3Ed__12_MoveNext_m64FDDC993B80C3831A646B0A02EDCF2EE0E83717 (void);
// 0x0000000C System.Object AchiveManager/<NoticeRoutine>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNoticeRoutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA3888002CF006005D9EADA6284686E8D1C03232 (void);
// 0x0000000D System.Void AchiveManager/<NoticeRoutine>d__12::System.Collections.IEnumerator.Reset()
extern void U3CNoticeRoutineU3Ed__12_System_Collections_IEnumerator_Reset_m17E48922158A0610B184C10BE7A6263CBAF9FC8C (void);
// 0x0000000E System.Object AchiveManager/<NoticeRoutine>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CNoticeRoutineU3Ed__12_System_Collections_IEnumerator_get_Current_m4A338FDAF00EADDD20AD3699C5499F13A75BFDCD (void);
// 0x0000000F System.Void AudioManager::Awake()
extern void AudioManager_Awake_m8138BCED4D692C83C95626A1A09AB46EA5205569 (void);
// 0x00000010 System.Void AudioManager::Init()
extern void AudioManager_Init_mF24127A535BC52E5305B531CA14711BBEA4AD5D5 (void);
// 0x00000011 System.Void AudioManager::PlayBgm(System.Boolean)
extern void AudioManager_PlayBgm_m18FF4DCD2BE27586DFFEFA4AD110DE48B13BDE9C (void);
// 0x00000012 System.Void AudioManager::EffextBgm(System.Boolean)
extern void AudioManager_EffextBgm_m14D0BA4702CBF787FFD387041C72AD4041EBD1D4 (void);
// 0x00000013 System.Void AudioManager::PlaySfx(AudioManager/Sfx)
extern void AudioManager_PlaySfx_mCA6993201FC24E7C72EABB6D8C7A55D0E40BBEF6 (void);
// 0x00000014 System.Void AudioManager::.ctor()
extern void AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6 (void);
// 0x00000015 System.Void Bullet::Awake()
extern void Bullet_Awake_m78CDB0B2B5399608F3DF94641B5EBAEE48D06749 (void);
// 0x00000016 System.Void Bullet::Init(System.Single,System.Int32,UnityEngine.Vector3)
extern void Bullet_Init_mAA3C1F52DDD71E852746776EF8D9E1E9CC1032C7 (void);
// 0x00000017 System.Void Bullet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Bullet_OnTriggerEnter2D_mC3642C0546BCF5AFC8036C6B77D66560C04685E8 (void);
// 0x00000018 System.Void Bullet::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Bullet_OnTriggerExit2D_m12575776F9E3C27C08B71456D8715D4106E38983 (void);
// 0x00000019 System.Void Bullet::.ctor()
extern void Bullet__ctor_m873C02F2114EA93A35E4392013AC831246756CBA (void);
// 0x0000001A System.Single Character::get_Speed()
extern void Character_get_Speed_mA6D7605A4CD8AFD6CE2378DAB295D2CDD3E50696 (void);
// 0x0000001B System.Single Character::get_WeaponSpeed()
extern void Character_get_WeaponSpeed_mB4F93E01CC2A591628F43D07324C618EFECC098B (void);
// 0x0000001C System.Single Character::get_WeaponRate()
extern void Character_get_WeaponRate_m6C0D6A9244802EECFFC292AA8E4FD48DA678744A (void);
// 0x0000001D System.Single Character::get_Damage()
extern void Character_get_Damage_m6A88BE900FAC59875F5585D6985DAFDBD15F8B45 (void);
// 0x0000001E System.Single Character::get_Count()
extern void Character_get_Count_mC0A43F21E0E6FD5A26366983C8AD92C1EC69C948 (void);
// 0x0000001F System.Void Character::.ctor()
extern void Character__ctor_m9D8D6104D9CB19DAE6866ECA929FFB0F2592DD19 (void);
// 0x00000020 System.Void Enemy::Awake()
extern void Enemy_Awake_mB58E74200229275689E6D9ADCDB6443D4E426624 (void);
// 0x00000021 System.Void Enemy::FixedUpdate()
extern void Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B (void);
// 0x00000022 System.Void Enemy::LateUpdate()
extern void Enemy_LateUpdate_m701A1B20AE7B72DE2216A343268C6813F35EF70E (void);
// 0x00000023 System.Void Enemy::OnEnable()
extern void Enemy_OnEnable_m2ABE6B2A5E1E193C6F9C780D165B75A1D9C9E140 (void);
// 0x00000024 System.Void Enemy::Init(SpawnData)
extern void Enemy_Init_m9A544B71561B9AB55BE3DBFAF3EAB3F734B8BEAE (void);
// 0x00000025 System.Void Enemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Enemy_OnTriggerEnter2D_m78DFD7AB12D1EC3E92F13D8E8C54701479ABF567 (void);
// 0x00000026 System.Collections.IEnumerator Enemy::KnockBack()
extern void Enemy_KnockBack_m44115920C2312C070C37D8E16254C5ECD1C5CE83 (void);
// 0x00000027 System.Void Enemy::Dead()
extern void Enemy_Dead_m8F63C5FBE635253D63EDFC4E9E1DD31D327EA391 (void);
// 0x00000028 System.Void Enemy::.ctor()
extern void Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933 (void);
// 0x00000029 System.Void Enemy/<KnockBack>d__17::.ctor(System.Int32)
extern void U3CKnockBackU3Ed__17__ctor_m096F86D36712276C6D6FB330998A89B111C216E5 (void);
// 0x0000002A System.Void Enemy/<KnockBack>d__17::System.IDisposable.Dispose()
extern void U3CKnockBackU3Ed__17_System_IDisposable_Dispose_m1DE9CD4C6A7EE6612182471C5820009108DEBEA9 (void);
// 0x0000002B System.Boolean Enemy/<KnockBack>d__17::MoveNext()
extern void U3CKnockBackU3Ed__17_MoveNext_mE0A2B97F2091049C58496C7E336EEA4F2BFA2AAC (void);
// 0x0000002C System.Object Enemy/<KnockBack>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockBackU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD277229A9F11B0D389A118B14FD61F5B4BC5FBE4 (void);
// 0x0000002D System.Void Enemy/<KnockBack>d__17::System.Collections.IEnumerator.Reset()
extern void U3CKnockBackU3Ed__17_System_Collections_IEnumerator_Reset_m8E392EA127A3F24ED9291F7F861AB0C7758BC0C1 (void);
// 0x0000002E System.Object Enemy/<KnockBack>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CKnockBackU3Ed__17_System_Collections_IEnumerator_get_Current_mDD3D0B5750843355A27254AEC03B0771CBEA4152 (void);
// 0x0000002F System.Void Follow::Awake()
extern void Follow_Awake_m0AD724237B5EC7458492852B525A8D4B6FCDC036 (void);
// 0x00000030 System.Void Follow::FixedUpdate()
extern void Follow_FixedUpdate_m43B8CC70C014AABA167B5932F7E555825E434619 (void);
// 0x00000031 System.Void Follow::.ctor()
extern void Follow__ctor_m7748C4BA7548A163F57B2692830B4FCEFB223B03 (void);
// 0x00000032 System.Void GameManager::Awake()
extern void GameManager_Awake_m4B6E8E2AF58C95C9A2A0C4637A34AE0892CB637F (void);
// 0x00000033 System.Void GameManager::GameStart(System.Int32)
extern void GameManager_GameStart_m3AE515BCB60DC82C75DB0878B7C2AB1533D842D2 (void);
// 0x00000034 System.Void GameManager::GameOver()
extern void GameManager_GameOver_mF1BD400E7F84A0B533A58E80ADA7CCB89C964625 (void);
// 0x00000035 System.Collections.IEnumerator GameManager::GameOverRoutine()
extern void GameManager_GameOverRoutine_mA1AE41628C875079BEE2B2A60FD79B60131BDEE1 (void);
// 0x00000036 System.Void GameManager::GameVictroy()
extern void GameManager_GameVictroy_m5EA81CF528D2D990828F259DBDB0437B6E799F24 (void);
// 0x00000037 System.Collections.IEnumerator GameManager::GameVictroyRoutine()
extern void GameManager_GameVictroyRoutine_mB971D114F93A19A502E83FE34DC35A50421831AF (void);
// 0x00000038 System.Void GameManager::GameRetry()
extern void GameManager_GameRetry_mED4B1C3F4133DE48D4FD8AE13D948DB14C5C8DE9 (void);
// 0x00000039 System.Void GameManager::GameQuit()
extern void GameManager_GameQuit_m0CEC9AFB0564372AFF7B926F831095351EED46E4 (void);
// 0x0000003A System.Void GameManager::Update()
extern void GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41 (void);
// 0x0000003B System.Void GameManager::GetExp()
extern void GameManager_GetExp_mD1674342AE085A462D67555D6848B1B350A55096 (void);
// 0x0000003C System.Void GameManager::Stop()
extern void GameManager_Stop_mEB1E5732E0A8C24C286CC7694CB610F7BDF4C0BD (void);
// 0x0000003D System.Void GameManager::Resume()
extern void GameManager_Resume_m950C220DCBB256CFE41136645CA3041CA0228394 (void);
// 0x0000003E System.Void GameManager::.ctor()
extern void GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368 (void);
// 0x0000003F System.Void GameManager/<GameOverRoutine>d__20::.ctor(System.Int32)
extern void U3CGameOverRoutineU3Ed__20__ctor_m4485D033B4EDE600452508F5EDC0CD6BCAD33C3A (void);
// 0x00000040 System.Void GameManager/<GameOverRoutine>d__20::System.IDisposable.Dispose()
extern void U3CGameOverRoutineU3Ed__20_System_IDisposable_Dispose_mE1C34F463A1FCCEE3AFE8C6DEF5F2C531F548E5B (void);
// 0x00000041 System.Boolean GameManager/<GameOverRoutine>d__20::MoveNext()
extern void U3CGameOverRoutineU3Ed__20_MoveNext_m7FF603DFC7F63B78826A07399CD9B2F447B44970 (void);
// 0x00000042 System.Object GameManager/<GameOverRoutine>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGameOverRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B63FD005A72CF0B6B7E152783F794EF88CAC81B (void);
// 0x00000043 System.Void GameManager/<GameOverRoutine>d__20::System.Collections.IEnumerator.Reset()
extern void U3CGameOverRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m83EECA9587783CA0993CD1D52D3341E24F90527C (void);
// 0x00000044 System.Object GameManager/<GameOverRoutine>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CGameOverRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_m8D22523AAA6793ED96030C355F832707E8056DD7 (void);
// 0x00000045 System.Void GameManager/<GameVictroyRoutine>d__22::.ctor(System.Int32)
extern void U3CGameVictroyRoutineU3Ed__22__ctor_m422F875057B6DF93F9A93E3AA47B95329B2B1C50 (void);
// 0x00000046 System.Void GameManager/<GameVictroyRoutine>d__22::System.IDisposable.Dispose()
extern void U3CGameVictroyRoutineU3Ed__22_System_IDisposable_Dispose_mB23CA05337C04AF2C13AA19855652D6FFE0EE5A6 (void);
// 0x00000047 System.Boolean GameManager/<GameVictroyRoutine>d__22::MoveNext()
extern void U3CGameVictroyRoutineU3Ed__22_MoveNext_m316F7E11F495C08B6D5F6C7BF0729C085667FA33 (void);
// 0x00000048 System.Object GameManager/<GameVictroyRoutine>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGameVictroyRoutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39BE652B569C4184350F2426016FEB452D8C41EF (void);
// 0x00000049 System.Void GameManager/<GameVictroyRoutine>d__22::System.Collections.IEnumerator.Reset()
extern void U3CGameVictroyRoutineU3Ed__22_System_Collections_IEnumerator_Reset_m4CF8898D1DFB7419C71D7789E95BC307C9F289D9 (void);
// 0x0000004A System.Object GameManager/<GameVictroyRoutine>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CGameVictroyRoutineU3Ed__22_System_Collections_IEnumerator_get_Current_m1A9F5BC109E66C4A26F0C4325311F4CC07BC389B (void);
// 0x0000004B System.Void Gear::Init(ItemData)
extern void Gear_Init_m5D61ED7E15CFC2897C62D7C5D0E1D1C0DA374655 (void);
// 0x0000004C System.Void Gear::LevelUp(System.Single)
extern void Gear_LevelUp_mC5A5C1338DC3497ABA4C7012335082AAF35339A9 (void);
// 0x0000004D System.Void Gear::ApplyGear()
extern void Gear_ApplyGear_mFCBBED00592D2E289A9B6BD2DA89F29633FEB2BD (void);
// 0x0000004E System.Void Gear::RateUp()
extern void Gear_RateUp_m43A0BF4F6A827E24A71B5D140A1ADC9FC23C603F (void);
// 0x0000004F System.Void Gear::SpeedUp()
extern void Gear_SpeedUp_mB114B2F77CA382DB5C27F1562CA302A80D59DA3C (void);
// 0x00000050 System.Void Gear::.ctor()
extern void Gear__ctor_m6567F8AACC0A96B6BE57A4737FFC5B0167D9CDDF (void);
// 0x00000051 System.Void Hand::Awake()
extern void Hand_Awake_m91E08D9613BF794DAE1AF79543447C040488CBFA (void);
// 0x00000052 System.Void Hand::LateUpdate()
extern void Hand_LateUpdate_m6B231678510B9FA48415E245DCFF4781FD9DB571 (void);
// 0x00000053 System.Void Hand::.ctor()
extern void Hand__ctor_m98922058C1A056306D560A36EAEDCE0247110FA8 (void);
// 0x00000054 System.Void HUD::Awake()
extern void HUD_Awake_m0C45C7727C41E8A92A17EE8C27F0A71F34F988FD (void);
// 0x00000055 System.Void HUD::LateUpdate()
extern void HUD_LateUpdate_m7C306F61E87AEFFCA0B8024E4B6133D49E6B84A0 (void);
// 0x00000056 System.Void HUD::.ctor()
extern void HUD__ctor_m82C674E1C84C730EE37CCDC237D5C45F278AFD23 (void);
// 0x00000057 System.Void Item::Awake()
extern void Item_Awake_m70FFBC1022548D82C7E37D465AE48D7052299214 (void);
// 0x00000058 System.Void Item::OnEnable()
extern void Item_OnEnable_m8CC09CD4A41545141A92BC77F9CABADAB30050A2 (void);
// 0x00000059 System.Void Item::OnClick()
extern void Item_OnClick_mD3B41F020DEDA6CD04C986ECA9AC21B4C024579B (void);
// 0x0000005A System.Void Item::.ctor()
extern void Item__ctor_m741D59B05082743C60D2F1149112B571E89CAFAF (void);
// 0x0000005B System.Void ItemData::.ctor()
extern void ItemData__ctor_mE45701993578016C9B2E439E01BFD91E13EBEAF0 (void);
// 0x0000005C System.Void LevelUp::Awake()
extern void LevelUp_Awake_mC1FB8864130F2B46F76524E28ABB18C0A013E805 (void);
// 0x0000005D System.Void LevelUp::Show()
extern void LevelUp_Show_m956213A74E0760239A05D7EE43958FAD3617B9EB (void);
// 0x0000005E System.Void LevelUp::Hide()
extern void LevelUp_Hide_m6975D03420E55E01D5F038BF70CB35141258426F (void);
// 0x0000005F System.Void LevelUp::Select(System.Int32)
extern void LevelUp_Select_mC74C8DBBBEA336A1AFF5B2CD8A53C130D97009D2 (void);
// 0x00000060 System.Void LevelUp::Next()
extern void LevelUp_Next_mC145B9844F1DD9DC07F3EADE1B5B1F2A376F4C7C (void);
// 0x00000061 System.Void LevelUp::.ctor()
extern void LevelUp__ctor_mB2C7E2D1AA8ACB988C3D22F1C11252E19F47E7AE (void);
// 0x00000062 System.Void Player::Awake()
extern void Player_Awake_m512A28E1559EB8AEEB2E1DB873F9F99FCC96BA67 (void);
// 0x00000063 System.Void Player::OnEnable()
extern void Player_OnEnable_mE0997C2EDE52E3BC53CCBF961D4FE375347F0906 (void);
// 0x00000064 System.Void Player::Update()
extern void Player_Update_m95E134A5EF1B5164EA281F61D7FA436F59BE3C9F (void);
// 0x00000065 System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_mEDDB0539FCD5145298CB87D4592DFFF98503AF9B (void);
// 0x00000066 System.Void Player::OnMove(UnityEngine.InputSystem.InputValue)
extern void Player_OnMove_mE479803C151BEF61894120A44548F9149C921D40 (void);
// 0x00000067 System.Void Player::LateUpdate()
extern void Player_LateUpdate_mECF802BE10DFD26BE9C6CFB651634E76316955B3 (void);
// 0x00000068 System.Void Player::OnCollisionStay2D(UnityEngine.Collision2D)
extern void Player_OnCollisionStay2D_mB9C5CA13ABD4994A3252FC7DF1EC4804ECBAC930 (void);
// 0x00000069 System.Void Player::.ctor()
extern void Player__ctor_m0A83E0706592FC871B0CF188B37AFC6649F3D85D (void);
// 0x0000006A System.Void PoolManager::Awake()
extern void PoolManager_Awake_m24E91438FC07109028C3E58A351E9185F1CA3B4F (void);
// 0x0000006B UnityEngine.GameObject PoolManager::Get(System.Int32)
extern void PoolManager_Get_m1466AD0033966D292097387888E0157DEB109004 (void);
// 0x0000006C System.Void PoolManager::.ctor()
extern void PoolManager__ctor_m9F56D45F7F8730BE0D42F25639C20D63A860CB6B (void);
// 0x0000006D System.Void Reposition::Awake()
extern void Reposition_Awake_m70B788E476A70EC58DE852A2C6B8147B6F40F26D (void);
// 0x0000006E System.Void Reposition::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Reposition_OnTriggerExit2D_m94A971649E1E5EF39C3B99B515CD9A769AD5E106 (void);
// 0x0000006F System.Void Reposition::.ctor()
extern void Reposition__ctor_mC6104DB2A7ACEB5A612A0F55898D801B1177E359 (void);
// 0x00000070 System.Void Result::Lose()
extern void Result_Lose_m75D6093192BE0A88F38994E0A9DBFE693C103628 (void);
// 0x00000071 System.Void Result::Win()
extern void Result_Win_m559E0BF08B320B32BD5597CA3E80975B5E0F83D5 (void);
// 0x00000072 System.Void Result::.ctor()
extern void Result__ctor_m2365D15DCC3F37EE327CD049B896733EDB00A91A (void);
// 0x00000073 System.Void Scanner::FixedUpdate()
extern void Scanner_FixedUpdate_mA9C90CA02E5E9752EB11BAEA883D4C0C873426E8 (void);
// 0x00000074 UnityEngine.Transform Scanner::GetNearest()
extern void Scanner_GetNearest_m5C46998AC2E23C906AE66078E77AD25BE3F62494 (void);
// 0x00000075 System.Void Scanner::.ctor()
extern void Scanner__ctor_mF6A4C3D6EEC092405DCE745CEEFD921A8DFFA81E (void);
// 0x00000076 System.Void Spawner::Awake()
extern void Spawner_Awake_m516B0FBB3B16DA918ABB2273636F28C0B1EC952A (void);
// 0x00000077 System.Void Spawner::Update()
extern void Spawner_Update_m6DAC21E4E2E5A3C46C3F3EE45A3358909808DDC5 (void);
// 0x00000078 System.Void Spawner::Spawn()
extern void Spawner_Spawn_m992EA9504776B40AE50865A0AB1DBFD1BA7F5956 (void);
// 0x00000079 System.Void Spawner::.ctor()
extern void Spawner__ctor_mBF592E8E9B5682687D8C28E73A64BF29B6BF2088 (void);
// 0x0000007A System.Void SpawnData::.ctor()
extern void SpawnData__ctor_m32C5F49A389019214B76C9284A1CA2036EC9322E (void);
// 0x0000007B System.Void Weapon::Awake()
extern void Weapon_Awake_m25895D001658578F1B4557C97E1D2C4E81127B69 (void);
// 0x0000007C System.Void Weapon::Update()
extern void Weapon_Update_m57E854EC3FC8CCF8FC7CB2D509855C8B025413A2 (void);
// 0x0000007D System.Void Weapon::LevelUp(System.Single,System.Int32)
extern void Weapon_LevelUp_mB2DC5033B6AA4EA63BAB4BC4D06E41EE02A87484 (void);
// 0x0000007E System.Void Weapon::Init(ItemData)
extern void Weapon_Init_m1ECC9E5A6E35FFDF5512768BF21DF50C456C4438 (void);
// 0x0000007F System.Void Weapon::Batch()
extern void Weapon_Batch_m802F9FEA0D9782980853456CDA300929C94E2B72 (void);
// 0x00000080 System.Void Weapon::Fire()
extern void Weapon_Fire_m0991C13F9C9A395330E5F600C1B36AF91EA01AFE (void);
// 0x00000081 System.Void Weapon::.ctor()
extern void Weapon__ctor_m6F053F0444AE8DF68EDA99C92CF944903C784332 (void);
static Il2CppMethodPointer s_methodPointers[129] = 
{
	AchiveManager_Awake_m3B62144FD46525FA94B4EC9584FF5E77AF6F4F8D,
	AchiveManager_Init_mDFD2CDB5E6E20EA55AEC14C6F14887F79C1C6C89,
	AchiveManager_Start_mF90E8828B0B06C403907F623928F8F712D437458,
	AchiveManager_UnlockCharacter_m1106130B02AD9E0E6C75602406502743E67F8E05,
	AchiveManager_LateUpdate_m6185BD47C1D9A1EABB210AF2C256C2B53E05BA3D,
	AchiveManager_CheckAchive_mA81C9732BE5A68EDC84ADF3CFB77A9C02F424353,
	AchiveManager_NoticeRoutine_m8C5A17AAF84709775BCDF66F6E5AF8C1A7268D0A,
	AchiveManager__ctor_mD0BE170E9BF1B624D5DF0A31716E115C42FD1022,
	U3CNoticeRoutineU3Ed__12__ctor_m77585D04350EEC243A8D5E588307EBFD4A5A885A,
	U3CNoticeRoutineU3Ed__12_System_IDisposable_Dispose_mAF2D87299483271E4BEF82A6776AEF2B1E07AC0B,
	U3CNoticeRoutineU3Ed__12_MoveNext_m64FDDC993B80C3831A646B0A02EDCF2EE0E83717,
	U3CNoticeRoutineU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAA3888002CF006005D9EADA6284686E8D1C03232,
	U3CNoticeRoutineU3Ed__12_System_Collections_IEnumerator_Reset_m17E48922158A0610B184C10BE7A6263CBAF9FC8C,
	U3CNoticeRoutineU3Ed__12_System_Collections_IEnumerator_get_Current_m4A338FDAF00EADDD20AD3699C5499F13A75BFDCD,
	AudioManager_Awake_m8138BCED4D692C83C95626A1A09AB46EA5205569,
	AudioManager_Init_mF24127A535BC52E5305B531CA14711BBEA4AD5D5,
	AudioManager_PlayBgm_m18FF4DCD2BE27586DFFEFA4AD110DE48B13BDE9C,
	AudioManager_EffextBgm_m14D0BA4702CBF787FFD387041C72AD4041EBD1D4,
	AudioManager_PlaySfx_mCA6993201FC24E7C72EABB6D8C7A55D0E40BBEF6,
	AudioManager__ctor_mA793A9DF6B975D03690B7C953972EFE41AE4D5E6,
	Bullet_Awake_m78CDB0B2B5399608F3DF94641B5EBAEE48D06749,
	Bullet_Init_mAA3C1F52DDD71E852746776EF8D9E1E9CC1032C7,
	Bullet_OnTriggerEnter2D_mC3642C0546BCF5AFC8036C6B77D66560C04685E8,
	Bullet_OnTriggerExit2D_m12575776F9E3C27C08B71456D8715D4106E38983,
	Bullet__ctor_m873C02F2114EA93A35E4392013AC831246756CBA,
	Character_get_Speed_mA6D7605A4CD8AFD6CE2378DAB295D2CDD3E50696,
	Character_get_WeaponSpeed_mB4F93E01CC2A591628F43D07324C618EFECC098B,
	Character_get_WeaponRate_m6C0D6A9244802EECFFC292AA8E4FD48DA678744A,
	Character_get_Damage_m6A88BE900FAC59875F5585D6985DAFDBD15F8B45,
	Character_get_Count_mC0A43F21E0E6FD5A26366983C8AD92C1EC69C948,
	Character__ctor_m9D8D6104D9CB19DAE6866ECA929FFB0F2592DD19,
	Enemy_Awake_mB58E74200229275689E6D9ADCDB6443D4E426624,
	Enemy_FixedUpdate_mC96E5B789335D23FC34546AD504EEBB0F054490B,
	Enemy_LateUpdate_m701A1B20AE7B72DE2216A343268C6813F35EF70E,
	Enemy_OnEnable_m2ABE6B2A5E1E193C6F9C780D165B75A1D9C9E140,
	Enemy_Init_m9A544B71561B9AB55BE3DBFAF3EAB3F734B8BEAE,
	Enemy_OnTriggerEnter2D_m78DFD7AB12D1EC3E92F13D8E8C54701479ABF567,
	Enemy_KnockBack_m44115920C2312C070C37D8E16254C5ECD1C5CE83,
	Enemy_Dead_m8F63C5FBE635253D63EDFC4E9E1DD31D327EA391,
	Enemy__ctor_mB6697627910F785A971C20C671DEFBA9D921D933,
	U3CKnockBackU3Ed__17__ctor_m096F86D36712276C6D6FB330998A89B111C216E5,
	U3CKnockBackU3Ed__17_System_IDisposable_Dispose_m1DE9CD4C6A7EE6612182471C5820009108DEBEA9,
	U3CKnockBackU3Ed__17_MoveNext_mE0A2B97F2091049C58496C7E336EEA4F2BFA2AAC,
	U3CKnockBackU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD277229A9F11B0D389A118B14FD61F5B4BC5FBE4,
	U3CKnockBackU3Ed__17_System_Collections_IEnumerator_Reset_m8E392EA127A3F24ED9291F7F861AB0C7758BC0C1,
	U3CKnockBackU3Ed__17_System_Collections_IEnumerator_get_Current_mDD3D0B5750843355A27254AEC03B0771CBEA4152,
	Follow_Awake_m0AD724237B5EC7458492852B525A8D4B6FCDC036,
	Follow_FixedUpdate_m43B8CC70C014AABA167B5932F7E555825E434619,
	Follow__ctor_m7748C4BA7548A163F57B2692830B4FCEFB223B03,
	GameManager_Awake_m4B6E8E2AF58C95C9A2A0C4637A34AE0892CB637F,
	GameManager_GameStart_m3AE515BCB60DC82C75DB0878B7C2AB1533D842D2,
	GameManager_GameOver_mF1BD400E7F84A0B533A58E80ADA7CCB89C964625,
	GameManager_GameOverRoutine_mA1AE41628C875079BEE2B2A60FD79B60131BDEE1,
	GameManager_GameVictroy_m5EA81CF528D2D990828F259DBDB0437B6E799F24,
	GameManager_GameVictroyRoutine_mB971D114F93A19A502E83FE34DC35A50421831AF,
	GameManager_GameRetry_mED4B1C3F4133DE48D4FD8AE13D948DB14C5C8DE9,
	GameManager_GameQuit_m0CEC9AFB0564372AFF7B926F831095351EED46E4,
	GameManager_Update_m7F29D8E933B8D21D2E67507979C0F12ACF87BB41,
	GameManager_GetExp_mD1674342AE085A462D67555D6848B1B350A55096,
	GameManager_Stop_mEB1E5732E0A8C24C286CC7694CB610F7BDF4C0BD,
	GameManager_Resume_m950C220DCBB256CFE41136645CA3041CA0228394,
	GameManager__ctor_mF453CED520617BFB65C52405A964E06CF17DB368,
	U3CGameOverRoutineU3Ed__20__ctor_m4485D033B4EDE600452508F5EDC0CD6BCAD33C3A,
	U3CGameOverRoutineU3Ed__20_System_IDisposable_Dispose_mE1C34F463A1FCCEE3AFE8C6DEF5F2C531F548E5B,
	U3CGameOverRoutineU3Ed__20_MoveNext_m7FF603DFC7F63B78826A07399CD9B2F447B44970,
	U3CGameOverRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B63FD005A72CF0B6B7E152783F794EF88CAC81B,
	U3CGameOverRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m83EECA9587783CA0993CD1D52D3341E24F90527C,
	U3CGameOverRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_m8D22523AAA6793ED96030C355F832707E8056DD7,
	U3CGameVictroyRoutineU3Ed__22__ctor_m422F875057B6DF93F9A93E3AA47B95329B2B1C50,
	U3CGameVictroyRoutineU3Ed__22_System_IDisposable_Dispose_mB23CA05337C04AF2C13AA19855652D6FFE0EE5A6,
	U3CGameVictroyRoutineU3Ed__22_MoveNext_m316F7E11F495C08B6D5F6C7BF0729C085667FA33,
	U3CGameVictroyRoutineU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39BE652B569C4184350F2426016FEB452D8C41EF,
	U3CGameVictroyRoutineU3Ed__22_System_Collections_IEnumerator_Reset_m4CF8898D1DFB7419C71D7789E95BC307C9F289D9,
	U3CGameVictroyRoutineU3Ed__22_System_Collections_IEnumerator_get_Current_m1A9F5BC109E66C4A26F0C4325311F4CC07BC389B,
	Gear_Init_m5D61ED7E15CFC2897C62D7C5D0E1D1C0DA374655,
	Gear_LevelUp_mC5A5C1338DC3497ABA4C7012335082AAF35339A9,
	Gear_ApplyGear_mFCBBED00592D2E289A9B6BD2DA89F29633FEB2BD,
	Gear_RateUp_m43A0BF4F6A827E24A71B5D140A1ADC9FC23C603F,
	Gear_SpeedUp_mB114B2F77CA382DB5C27F1562CA302A80D59DA3C,
	Gear__ctor_m6567F8AACC0A96B6BE57A4737FFC5B0167D9CDDF,
	Hand_Awake_m91E08D9613BF794DAE1AF79543447C040488CBFA,
	Hand_LateUpdate_m6B231678510B9FA48415E245DCFF4781FD9DB571,
	Hand__ctor_m98922058C1A056306D560A36EAEDCE0247110FA8,
	HUD_Awake_m0C45C7727C41E8A92A17EE8C27F0A71F34F988FD,
	HUD_LateUpdate_m7C306F61E87AEFFCA0B8024E4B6133D49E6B84A0,
	HUD__ctor_m82C674E1C84C730EE37CCDC237D5C45F278AFD23,
	Item_Awake_m70FFBC1022548D82C7E37D465AE48D7052299214,
	Item_OnEnable_m8CC09CD4A41545141A92BC77F9CABADAB30050A2,
	Item_OnClick_mD3B41F020DEDA6CD04C986ECA9AC21B4C024579B,
	Item__ctor_m741D59B05082743C60D2F1149112B571E89CAFAF,
	ItemData__ctor_mE45701993578016C9B2E439E01BFD91E13EBEAF0,
	LevelUp_Awake_mC1FB8864130F2B46F76524E28ABB18C0A013E805,
	LevelUp_Show_m956213A74E0760239A05D7EE43958FAD3617B9EB,
	LevelUp_Hide_m6975D03420E55E01D5F038BF70CB35141258426F,
	LevelUp_Select_mC74C8DBBBEA336A1AFF5B2CD8A53C130D97009D2,
	LevelUp_Next_mC145B9844F1DD9DC07F3EADE1B5B1F2A376F4C7C,
	LevelUp__ctor_mB2C7E2D1AA8ACB988C3D22F1C11252E19F47E7AE,
	Player_Awake_m512A28E1559EB8AEEB2E1DB873F9F99FCC96BA67,
	Player_OnEnable_mE0997C2EDE52E3BC53CCBF961D4FE375347F0906,
	Player_Update_m95E134A5EF1B5164EA281F61D7FA436F59BE3C9F,
	Player_FixedUpdate_mEDDB0539FCD5145298CB87D4592DFFF98503AF9B,
	Player_OnMove_mE479803C151BEF61894120A44548F9149C921D40,
	Player_LateUpdate_mECF802BE10DFD26BE9C6CFB651634E76316955B3,
	Player_OnCollisionStay2D_mB9C5CA13ABD4994A3252FC7DF1EC4804ECBAC930,
	Player__ctor_m0A83E0706592FC871B0CF188B37AFC6649F3D85D,
	PoolManager_Awake_m24E91438FC07109028C3E58A351E9185F1CA3B4F,
	PoolManager_Get_m1466AD0033966D292097387888E0157DEB109004,
	PoolManager__ctor_m9F56D45F7F8730BE0D42F25639C20D63A860CB6B,
	Reposition_Awake_m70B788E476A70EC58DE852A2C6B8147B6F40F26D,
	Reposition_OnTriggerExit2D_m94A971649E1E5EF39C3B99B515CD9A769AD5E106,
	Reposition__ctor_mC6104DB2A7ACEB5A612A0F55898D801B1177E359,
	Result_Lose_m75D6093192BE0A88F38994E0A9DBFE693C103628,
	Result_Win_m559E0BF08B320B32BD5597CA3E80975B5E0F83D5,
	Result__ctor_m2365D15DCC3F37EE327CD049B896733EDB00A91A,
	Scanner_FixedUpdate_mA9C90CA02E5E9752EB11BAEA883D4C0C873426E8,
	Scanner_GetNearest_m5C46998AC2E23C906AE66078E77AD25BE3F62494,
	Scanner__ctor_mF6A4C3D6EEC092405DCE745CEEFD921A8DFFA81E,
	Spawner_Awake_m516B0FBB3B16DA918ABB2273636F28C0B1EC952A,
	Spawner_Update_m6DAC21E4E2E5A3C46C3F3EE45A3358909808DDC5,
	Spawner_Spawn_m992EA9504776B40AE50865A0AB1DBFD1BA7F5956,
	Spawner__ctor_mBF592E8E9B5682687D8C28E73A64BF29B6BF2088,
	SpawnData__ctor_m32C5F49A389019214B76C9284A1CA2036EC9322E,
	Weapon_Awake_m25895D001658578F1B4557C97E1D2C4E81127B69,
	Weapon_Update_m57E854EC3FC8CCF8FC7CB2D509855C8B025413A2,
	Weapon_LevelUp_mB2DC5033B6AA4EA63BAB4BC4D06E41EE02A87484,
	Weapon_Init_m1ECC9E5A6E35FFDF5512768BF21DF50C456C4438,
	Weapon_Batch_m802F9FEA0D9782980853456CDA300929C94E2B72,
	Weapon_Fire_m0991C13F9C9A395330E5F600C1B36AF91EA01AFE,
	Weapon__ctor_m6F053F0444AE8DF68EDA99C92CF944903C784332,
};
static const int32_t s_InvokerIndices[129] = 
{
	6984,
	6984,
	6984,
	6984,
	6984,
	5491,
	6857,
	6984,
	5491,
	6984,
	6756,
	6857,
	6984,
	6857,
	6984,
	6984,
	5420,
	5420,
	5491,
	6984,
	6984,
	1639,
	5520,
	5520,
	6984,
	12685,
	12685,
	12685,
	12685,
	12685,
	6984,
	6984,
	6984,
	6984,
	6984,
	5520,
	5520,
	6857,
	6984,
	6984,
	5491,
	6984,
	6756,
	6857,
	6984,
	6857,
	6984,
	6984,
	6984,
	6984,
	5491,
	6984,
	6857,
	6984,
	6857,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	5491,
	6984,
	6756,
	6857,
	6984,
	6857,
	5491,
	6984,
	6756,
	6857,
	6984,
	6857,
	5520,
	5574,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	5491,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	5520,
	6984,
	5520,
	6984,
	6984,
	4909,
	6984,
	6984,
	5520,
	6984,
	6984,
	6984,
	6984,
	6984,
	6857,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	6984,
	3160,
	5520,
	6984,
	6984,
	6984,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	129,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
